package com.enderzombi102.wthitplugins.plugin.vanilla;

import com.enderzombi102.wthitplugins.Const;
import com.enderzombi102.wthitplugins.Util;
import mcp.mobius.waila.api.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;


public class BreedableStatsPlugin implements IWailaPlugin, IEntityComponentProvider, IServerDataProvider<Entity> {
	private static final Identifier BREEDING_COOLDOWN_ID = Const.getId( "breeding_cooldown");
	private static final Identifier GROWING_COOLDOWN_ID = Const.getId( "growing_cooldown");
	private static final Identifier BREEDING_ITEM_ID = Const.getId( "breeding_item");

	@Override
	public void register(IRegistrar registrar) {
		registrar.addConfig(BREEDING_COOLDOWN_ID, true);
		registrar.addConfig(GROWING_COOLDOWN_ID, true);
		registrar.addConfig(BREEDING_ITEM_ID, true);

		registrar.addEntityData(this, PassiveEntity.class);
		registrar.addComponent(this, TooltipPosition.BODY, PassiveEntity.class);
	}

	@Override
	public void appendServerData(NbtCompound nbt, ServerPlayerEntity player, World world, Entity target) {
		if (target instanceof PassiveEntity entity) {
			nbt.putInt( "WthitPluginsBreedingAge", entity.getBreedingAge() );
		}
	}

	@Override
	public void appendBody(ITooltip tooltip, IEntityAccessor accessor, IPluginConfig config) {
		if ( accessor.getServerData().contains("WthitPluginsBreedingAge") ) {
			final int breedingAge = accessor.getServerData().getInt("WthitPluginsBreedingAge");

			if ( breedingAge < 0 && config.getBoolean(GROWING_COOLDOWN_ID) ) {
				// Time until Adulthood
				tooltip.add(
						Util.getFullText(
								"growingage",
								Util.toFriendlyMinutes( Math.abs( breedingAge ) )
						)
				);
			} else if ( breedingAge > 0 && config.getBoolean(BREEDING_COOLDOWN_ID) ) {
				// Breedable in
				tooltip.add(
						Util.getFullText(
								"breedingtime",
								Util.toFriendlyMinutes( breedingAge )
						)
				);
			}

			if (
					config.getBoolean(BREEDING_ITEM_ID) &&
					breedingAge == 0 &&
					accessor.getPlayer() != null &&
					accessor.getEntity() instanceof AnimalEntity animal
			) {
				final ItemStack heldItem = accessor.getPlayer().getMainHandStack();

				if (! heldItem.isEmpty() && !animal.isBaby() && animal.isBreedingItem(heldItem) ) {
					tooltip.add(
							Util.getFullText("breedingitem").formatted( Formatting.YELLOW )
					);
				}
			}

		}
	}
}
