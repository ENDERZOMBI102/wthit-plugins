package com.enderzombi102.wthitplugins.plugin.generic;

import com.enderzombi102.wthitplugins.ClientWthitPlugins;
import com.enderzombi102.wthitplugins.Const;
import com.enderzombi102.wthitplugins.Util;
import com.enderzombi102.wthitplugins.mixin.KeyBindingAccessor;
import mcp.mobius.waila.api.*;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.InputUtil;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AuthorPlugin implements IWailaPlugin, IEntityComponentProvider, IBlockComponentProvider, IEventListener {
	private static final Identifier AUTHOR_ID = Const.getId( "author" );
	private static final HashMap<String, String> MOD_AUTHORS = new HashMap<>() {{
		put( "minecraft", "mojang" );
	}};
	public static final List<AuthorProvider> PROVIDERS = new ArrayList<>() {{
		add( modid -> {
			var container = FabricLoader.getInstance().getModContainer( modid );
			if ( container.isPresent() ) {
				var buf = new StringBuilder();
				for ( var person : container.get().getMetadata().getAuthors() )
					buf.append( " " ).append( person.getName() ).append( "," );
				if ( buf.length() > 0 )
					return buf.substring( 0, buf.length() - 1 );
			}
			return null;
		} );
	}};

	@Override
	public void register( IRegistrar registrar ) {
		registrar.addConfig( AUTHOR_ID, true );
		registrar.addComponent( (IEntityComponentProvider) this, TooltipPosition.TAIL, LivingEntity.class );
		registrar.addComponent( (IBlockComponentProvider) this, TooltipPosition.TAIL, Block.class );
		registrar.addEventListener( this );
	}

	@Override
	public void appendTail( ITooltip tooltip, IBlockAccessor accessor, IPluginConfig config ) {
		if ( config.getBoolean( AUTHOR_ID ) && shouldDisplay() ) {
			tooltip.add(
				Util.getFullText(
					"madeby",
					getAuthors(
						Registry.BLOCK.getId( accessor.getBlock() ).getNamespace()
					)
				).formatted( Formatting.BLUE )
			);
		}
	}

	@Override
	public void appendTail( ITooltip tooltip, IEntityAccessor accessor, IPluginConfig config ) {
		if ( config.getBoolean( AUTHOR_ID ) && shouldDisplay() ) {
			tooltip.add(
				Util.getFullText(
					"madeby",
					getAuthors(
						Registry.ENTITY_TYPE.getId( accessor.getEntity().getType() ).getNamespace()
					)
				).formatted( Formatting.BLUE )
			);
		}
	}

	@Override
	public @Nullable String getHoveredItemModName( ItemStack stack, IPluginConfig config ) {
		if ( config.getBoolean( AUTHOR_ID ) && shouldDisplay() ) {
			return getAuthors( Registry.ITEM.getId( stack.getItem() ).getNamespace() );
		}
		return null;
	}

	private static boolean shouldDisplay() {
		return ( !ClientWthitPlugins.getInstance().showAuthorsBinding.isUnbound() ) && InputUtil.isKeyPressed(
			MinecraftClient.getInstance().getWindow().getHandle(),
			( (KeyBindingAccessor) ClientWthitPlugins.getInstance().showAuthorsBinding ).getBoundKey().getCode()
		);
	}

	private static @Nullable String getAuthors( String modid ) {
		MOD_AUTHORS.computeIfAbsent( modid, key -> {
			for ( var provider : PROVIDERS ) {
				var authors = provider.getAuthorsString( modid );
				if ( authors != null )
					return authors;
			}
			return "N/D";
		} );
		return MOD_AUTHORS.get( modid );
	}

	public interface AuthorProvider {
		@Nullable String getAuthorsString( @NotNull String modid );
	}
}
