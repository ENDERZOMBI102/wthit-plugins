WTHIT Plugins
-
Plugin based off [WAWLA](https://github.com/Darkhax-Minecraft/WAWLA),
[Waila Harvestability](https://github.com/squeek502/WailaHarvestability),
[Waila Features](https://github.com/way2muchnoise/WAILA-features) and
[Waila Plugins](https://github.com/tterrag1098/WAILAPlugins).

Here's a list of features, and where it was originated

| Feature              | Origin                                                                                                                       |
|----------------------|------------------------------------------------------------------------------------------------------------------------------|
| Break Progress       | [WAWLA](https://github.com/Darkhax-Minecraft/WAWLA)                                                                          |
| Breedable Stats      | [WAWLA](https://github.com/Darkhax-Minecraft/WAWLA)                                                                          |
| Show Mod Author      | [Waila Features](https://github.com/way2muchnoise/WAILA-features)                                                            |
| Harvestability Stats | [WAWLA](https://github.com/Darkhax-Minecraft/WAWLA), [WailaHarvestability](https://github.com/squeek502/WailaHarvestability) |
| Armor Stats          | [WAWLA](https://github.com/Darkhax-Minecraft/WAWLA)                                                                          |
