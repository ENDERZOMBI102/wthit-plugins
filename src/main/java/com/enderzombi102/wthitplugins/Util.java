package com.enderzombi102.wthitplugins;

import net.minecraft.text.TranslatableText;

import java.util.concurrent.TimeUnit;

public final class Util {

	public static TranslatableText getFullText( String key, Object... args ) {
		return new TranslatableText( "info.wthit-plugins." + key, args );
	}

	public static String toFriendlyMinutes(int ticks) {
		final long seconds = ticks / 20;
		final long minutes = TimeUnit.SECONDS.toMinutes( seconds );
		return "" + minutes + ":" + ( seconds - TimeUnit.MINUTES.toSeconds( minutes ) );
	}

}
