package com.enderzombi102.wthitplugins;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.util.Identifier;

public final class Const {

	public static final String MOD_ID = "wthit-plugins";
	public static final String MOD_NAME = "wthit plugins";
	public static final String MOD_VERSION = FabricLoader.getInstance()
			.getModContainer(MOD_ID)
			.orElseThrow()
			.getMetadata()
			.getVersion()
			.getFriendlyString();

	public static Identifier getId(String path) {
		return new Identifier( MOD_ID, path );
	}
}
