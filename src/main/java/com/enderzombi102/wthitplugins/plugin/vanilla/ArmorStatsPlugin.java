package com.enderzombi102.wthitplugins.plugin.vanilla;

import com.enderzombi102.wthitplugins.Const;
import com.enderzombi102.wthitplugins.Util;
import mcp.mobius.waila.api.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class ArmorStatsPlugin implements IWailaPlugin, IEntityComponentProvider, IServerDataProvider<Entity> {
	private static final Identifier ARMOR_STATS_ID = Const.getId("armor_stats");

	@Override
	public void register(IRegistrar registrar) {
		// TODO: Make this an enum to display armor as the bar
		registrar.addConfig(ARMOR_STATS_ID, true);
		registrar.addEntityData(this, LivingEntity.class);
		registrar.addComponent(this, TooltipPosition.BODY, LivingEntity.class);
	}

	@Override
	public void appendBody(ITooltip tooltip, IEntityAccessor accessor, IPluginConfig config) {
		if ( config.getBoolean(ARMOR_STATS_ID) ) {
			final int armorPoints = accessor.getServerData().getInt("WthitPluginsArmor");

			if ( armorPoints > 0 ) {
				tooltip.add( Util.getFullText( "armor", armorPoints ) );
			}
		}
	}

	@Override
	public void appendServerData(NbtCompound nbt, ServerPlayerEntity player, World world, Entity target) {
		if ( target instanceof LivingEntity entity) {
			nbt.putInt(
					"WthitPluginsArmor",
					entity.getArmor()
			);
		}
	}
}
